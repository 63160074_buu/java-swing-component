/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.java_swing_component;

import javax.swing.*;

/**
 *
 * @author f
 */
public class CheckBoxExample1 {

    CheckBoxExample1() {
        JFrame f = new JFrame("CheckBox Example");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JCheckBox checkBox1 = new JCheckBox("C++");
        checkBox1.setBounds(100, 100, 100, 50);
        JCheckBox checkBox2 = new JCheckBox("Java", true);
        checkBox2.setBounds(100, 150, 100, 50);
        f.add(checkBox1);
        f.add(checkBox2);
        f.setSize(400, 400);
        f.setLayout(null);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new CheckBoxExample1();
    }

}
