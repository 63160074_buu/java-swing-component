/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.java_swing_component;

import javax.swing.*;

/**
 *
 * @author f
 */
public class Simple1 {

    JFrame f;

    Simple1() {
        f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JButton b = new JButton("Click");  
        b.setBounds(130, 100, 100, 40);
        f.add(b);
        f.setSize(400, 500); 
        f.setLayout(null);  
        f.setVisible(true); 
    }

    public static void main(String[] args) {
        new Simple1();
    }
}
