/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.java_swing_component;

import javax.swing.*;

/**
 *
 * @author f
 */
public class ComboBoxExample1 {

    JFrame f;

    ComboBoxExample1() {
        f = new JFrame("ComboBox Example");
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        String country[] = {"India", "Aus", "U.S.A", "England", "Newzealand","Thailand"};
        JComboBox cb = new JComboBox(country);
        cb.setBounds(50, 50, 90, 20);
        f.add(cb);
        f.setLayout(null);
        f.setSize(400, 500);
        f.setVisible(true);
    }

    public static void main(String[] args) {
        new ComboBoxExample1();
    }
}
