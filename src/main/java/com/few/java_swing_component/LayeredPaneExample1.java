/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.java_swing_component;

import javax.swing.*;
import java.awt.*;

/**
 *
 * @author f
 */
public class LayeredPaneExample1 extends JFrame {

    public LayeredPaneExample1() {
        super("LayeredPane Example");
        setSize(200, 200);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        JLayeredPane pane = getLayeredPane();  
        JButton top = new JButton();
        top.setBackground(Color.white);
        top.setBounds(20, 20, 50, 50);
        JButton middle = new JButton();
        middle.setBackground(Color.red);
        middle.setBounds(40, 40, 50, 50);
        JButton bottom = new JButton();
        bottom.setBackground(Color.cyan);
        bottom.setBounds(60, 60, 50, 50);
        pane.add(bottom, new Integer(1));
        pane.add(middle, new Integer(2));
        pane.add(top, new Integer(3));
    }

    public static void main(String[] args) {
        LayeredPaneExample1 panel = new LayeredPaneExample1();
        panel.setVisible(true);
    }
}
