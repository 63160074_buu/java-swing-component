/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.java_swing_component;

import java.awt.*;
import javax.swing.JFrame;

/**
 *
 * @author f
 */
public class IconExample1 {

    IconExample1() {
        JFrame f = new JFrame();
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        Image icon = Toolkit.getDefaultToolkit().getImage("C:\\Users\\f\\Downloads\\pictures\\NEW\\X3.png");
        f.setIconImage(icon);
        f.setLayout(null);
        f.setSize(400, 400);
        f.setVisible(true);
    }

    public static void main(String args[]) {
        new IconExample1();
    }
}
