/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.few.java_swing_component;

import javax.swing.*;

/**
 *
 * @author f
 */
public class ProgressBarExample1 extends JFrame {

    JProgressBar jb;
    int i = 0, num = 0;

    ProgressBarExample1() {
        jb = new JProgressBar(0, 2000);
        jb.setBounds(40, 40, 160, 30);
        jb.setValue(0);
        jb.setStringPainted(true);
        add(jb);
        setSize(250, 150);
        setLayout(null);
        setDefaultCloseOperation(EXIT_ON_CLOSE);
    }

    public void iterate() {
        while (i <= 2000) {
            jb.setValue(i);
            i = i + 20;
            try {
                Thread.sleep(150);
            } catch (Exception e) {
            }
        }
    }

    public static void main(String[] args) {
        ProgressBarExample1 m = new ProgressBarExample1();
        m.setVisible(true);
        m.iterate();
    }
}
